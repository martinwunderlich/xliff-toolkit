package net.sf.okapi.lib.xliff2.writer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.StringWriter;

import javax.xml.namespace.QName;

import net.sf.okapi.lib.xliff2.Const;
import net.sf.okapi.lib.xliff2.changeTracking.ChangeTrack;
import net.sf.okapi.lib.xliff2.changeTracking.Item;
import net.sf.okapi.lib.xliff2.changeTracking.Revision;
import net.sf.okapi.lib.xliff2.changeTracking.Revisions;
import net.sf.okapi.lib.xliff2.core.Directionality;
import net.sf.okapi.lib.xliff2.core.ExtContent;
import net.sf.okapi.lib.xliff2.core.ExtElement;
import net.sf.okapi.lib.xliff2.core.ExtElements;
import net.sf.okapi.lib.xliff2.core.Fragment;
import net.sf.okapi.lib.xliff2.core.MidFileData;
import net.sf.okapi.lib.xliff2.core.Note;
import net.sf.okapi.lib.xliff2.core.Note.AppliesTo;
import net.sf.okapi.lib.xliff2.core.Part.GetTarget;
import net.sf.okapi.lib.xliff2.core.Segment;
import net.sf.okapi.lib.xliff2.core.StartFileData;
import net.sf.okapi.lib.xliff2.core.StartGroupData;
import net.sf.okapi.lib.xliff2.core.StartXliffData;
import net.sf.okapi.lib.xliff2.core.TagType;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.reader.XLIFFReader;
import net.sf.okapi.lib.xliff2.test.U;

import org.junit.Test;

public class XLIFFWriterTest {
	
	private static final String STARTDOC = "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" ";

	@Test
	public void testEmptyDoc () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		writer.create(strWriter, "en");

		writer.writeStartDocument(null, null);
		writer.writeStartGroup(null);
		writer.writeEndGroup();
		writer.writeEndDocument();
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\">\n"
			+ "<file id=\"f1\">\n<group id=\"g1\">\n</group>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testDirectoryCreation () {
		String root = U.getParentDir(this, "/example.xlf");
		String path = root+"/dir1/dir two/file with spaces.xlf";
		File file = new File(path);
		file.delete();
		file.getParentFile().delete();
		
		XLIFFWriter writer = new XLIFFWriter();
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		writer.create(file, "en");
		writer.writeStartDocument(null, null);
		writer.writeStartGroup(null);
		writer.writeEndGroup();
		writer.writeEndDocument();
		writer.close();
		assertTrue(file.exists());
		XLIFFReader.validate(file);
	}

	@Test
	public void testPresettingFileId () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		writer.create(strWriter, "en");

		writer.writeStartDocument(null, null);
		StartFileData sfd = new StartFileData("myFileId");
		writer.setStartFileData(sfd);
		// writeStartFile() is called automatically
		writer.writeStartGroup(null);
		writer.writeEndGroup();
		writer.writeEndDocument();
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\">\n"
			+ "<file id=\"myFileId\">\n<group id=\"g1\">\n</group>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testPresettingFileStartButNoId () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		writer.create(strWriter, "en");

		writer.writeStartDocument(null, null);
		StartFileData sfd = new StartFileData(null);
		sfd.setOriginal("original");
		writer.setStartFileData(sfd);
		// writeStartFile() is called automatically
		writer.writeStartGroup(null);
		writer.writeEndGroup();
		writer.writeEndDocument();
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\">\n"
			+ "<file id=\"f1\" original=\"original\">\n<group id=\"g1\">\n</group>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testWithExtensionAttributes () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		writer.create(strWriter, "en");

		StartXliffData dd = new StartXliffData(null);
		dd.setNamespace("its", Const.NS_ITS);
		dd.getExtAttributes().setAttribute(Const.NS_ITS, "version", "2.0");
		writer.writeStartDocument(dd, "comment");
		writer.writeStartGroup(null);
		writer.writeEndGroup();
		writer.writeEndDocument();
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\""
			+ " xmlns:its=\"http://www.w3.org/2005/11/its\" its:version=\"2.0\">\n"
			+ "<!-- comment -->\n"
			+ "<file id=\"f1\">\n<group id=\"g1\">\n</group>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testCloseable () {
		Unit unit = new Unit("id");
		unit.appendSegment().setSource("text");
		StringWriter strWriter = new StringWriter();
		try ( XLIFFWriter writer = new XLIFFWriter() ) {
			writer.create(strWriter, "fr-CA");
			writer.writeUnit(unit);
		}
		Unit ures = U.getUnit(U.getEvents(strWriter.toString()));
		assertEquals("text", ures.getPart(0).getSource().toString());
	}
	
	@Test
	public void testOneUnitWithEmpties () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		writer.create(strWriter, "en");

		// Empty so no output
		writer.writeUnit(new Unit("id1")); 
		// Empty part, so output
		Unit unit = new Unit("id2");
		unit.appendIgnorable();
		writer.writeUnit(unit);
		// One empty segment so output
		unit = new Unit("id3");
		unit.appendSegment(); 
		writer.writeUnit(unit);
		writer.writeEndDocument();
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id2\">\n"
			+ "<ignorable>\n"
			+ "<source></source>\n"
			+ "</ignorable>\n"
			+ "<segment>\n"
			+ "<source></source>\n"
			+ "</segment>\n"
			+ "</unit>\n"
			+ "<unit id=\"id3\">\n"
			+ "<segment>\n"
			+ "<source></source>\n"
			+ "</segment>\n"
			+ "</unit>\n"
			+ "</file>\n"
			+ "</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testTwoSegments () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.create(strWriter, "en");
		
		Unit unit = new Unit("id");
		unit.appendSegment().setSource("Source 1.");
		unit.appendSegment().setSource("Source 2.");
		writer.writeUnit(unit);
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<segment>\n"
			+ "<source>Source 1.</source>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source>Source 2.</source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testDirectionality () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.create(strWriter, "en", "fr");
		
		StartFileData sd = new StartFileData("idsd");
		sd.setSourceDir(Directionality.RTL);
		sd.setTargetDir(Directionality.RTL);
		writer.writeStartFile(sd);
		// Unit 1 -> same directionality as the file
		Unit unit = new Unit("id1", sd);
		unit.appendSegment().setSource("text1");
		assertEquals(Directionality.RTL, unit.getSourceDir());
		assertEquals(Directionality.RTL, unit.getTargetDir());
		writer.writeUnit(unit);
		// Unit 2 -> unit srcDir as LTR
		unit = new Unit("id2", sd);
		unit.appendSegment().setSource("text2");
		unit.setSourceDir(Directionality.LTR);
		assertEquals(Directionality.LTR, unit.getSourceDir());
		assertEquals(Directionality.RTL, unit.getTargetDir());
		assertEquals(Directionality.LTR, unit.getPart(0).getSource().getDir(true));
		// Unit 2, segment 2 -> source as RTL, target as RTL
		Segment seg = unit.appendSegment();
		seg.setSource("text3");
		seg.getSource().setDir(Directionality.RTL);
		seg.setTarget("trgText3");
		seg.getTarget().setDir(Directionality.RTL);
		// Check that the source dir is different for the two segments
		assertEquals(Directionality.RTL, unit.getPart(1).getSource().getDir(true));
		assertEquals(Directionality.LTR, unit.getPart(0).getSource().getDir(true));
		// Output
		writer.writeUnit(unit);
//TODO: the output is valid but does not reflect the intent as the source/target don't have dir anymore
// See https://code.google.com/p/okapi-xliff-toolkit/issues/detail?id=5
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\" trgLang=\"fr\">\n"
			+ "<file id=\"idsd\" srcDir=\"rtl\" trgDir=\"rtl\">\n"
			+ "<unit id=\"id1\">\n"
			+ "<segment>\n"
			+ "<source>text1</source>\n"
			+ "</segment>\n"
			+ "</unit>\n"
			+ "<unit id=\"id2\" srcDir=\"ltr\">\n"
			+ "<segment>\n"
			+ "<source>text2</source>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source>text3</source>\n"
			+ "<target>trgText3</target>\n" // RTL is inherited from file (not changed by unit)
			+ "</segment>\n"
			+ "</unit>\n"
			+ "</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testIsolatedAttributeSameUnit () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en");
		writer.setLineBreak("\n");
		writer.setWithOriginalData(false);
		
		Unit unit = new Unit("id");
		Segment seg = (Segment)unit.appendSegment();
		seg.getSource().append(TagType.OPENING, "B1", "<B>", false); // End in next segment
		seg.getSource().append(TagType.OPENING, "U1", "<U>", false); // End in this segment
		seg.getSource().append(TagType.CLOSING, "I1", "</I>", false); // No start
		seg.getSource().append("s1. ");
		seg.getSource().append(TagType.CLOSING, "U1", "</U>", false);
		seg = (Segment)unit.appendSegment();
		seg.getSource().append("s2.");
		seg.getSource().append(TagType.CLOSING, "B1", "</B>", false);
		// No end of Italic
		writer.writeUnit(unit);
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<segment>\n"
			+ "<source><sc id=\"B1\" canOverlap=\"no\"/><sc id=\"U1\" canOverlap=\"no\"/><ec id=\"I1\" isolated=\"yes\" canOverlap=\"no\"/>s1. <ec startRef=\"U1\" canOverlap=\"no\"/></source>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source>s2.<ec startRef=\"B1\" canOverlap=\"no\"/></source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

//	@Test
//	public void testSegmentWithMatches () {
//		XLIFFWriter writer = new XLIFFWriter();
//		StringWriter strWriter = new StringWriter();
//		writer.create(strWriter, "en");
//		writer.setLineBreak("\n");
//		writer.setInlineStyle(OriginalDataStyle.OUTSIDE);
//		
//		Unit unit = new Unit("id");
//		Segment seg = unit.appendNewSegment();
//		seg.setSource("Source 1.");
//		seg.addCandidate(createAlternate("seg", 77.54f));
//		unit.addCandidate(createAlternate("unit", 99.00f));
//		writer.writeUnit(unit);
//		
//		writer.close();
//		assertEquals("<?xml version=\"1.0\"?>\n"
//			+ STARTDOC+"srcLang=\"en\">\n"
//			+ "<file id=\"f1\">\n"
//			+ "<unit id=\"id\">\n"
//			+ "<segment>\n"
//			+ "<source>Source 1.</source>\n"
//			+ "<m:matches>\n"
//			+ "<m:match similarity=\"77.54\" type=\"tm\" origin=\"ori\">\n"
//			+ "<source>seg-text<ph id=\"1\" dataRef=\"d1\"/></source>\n"
//			+ "<target>SEG-TEXT<ph id=\"1\" dataRef=\"d1\"/></target>\n"
//			+ "<originalData>\n"
//			+ "<data id=\"d1\">&lt;br/></data>\n"
//			+ "</originalData>\n"
//			+ "</m:match>\n"
//			+ "</m:matches>\n"
//			+ "</segment>\n"
//			+ "<m:matches>\n"
//			+ "<m:match similarity=\"99\" type=\"tm\" origin=\"ori\">\n"
//			+ "<source>unit-text<ph id=\"1\" dataRef=\"d1\"/></source>\n"
//			+ "<target>UNIT-TEXT<ph id=\"1\" dataRef=\"d1\"/></target>\n"
//			+ "<originalData>\n"
//			+ "<data id=\"d1\">&lt;br/></data>\n"
//			+ "</originalData>\n"
//			+ "</m:match>\n"
//			+ "</m:matches>\n"
//			+ "</unit>\n</file>\n</xliff>\n",
//			strWriter.toString());
//	}

	@Test
	public void testNotes () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en");
		writer.setLineBreak("\n");
		writer.setUseIndentation(true);
		writer.setWithOriginalData(true);
		
		MidFileData midFileData = new MidFileData();
		midFileData.addNote(new Note("File note", AppliesTo.UNDEFINED));
		writer.writeMidFile(midFileData);

		Note note = new Note("Group note", AppliesTo.SOURCE);
		note.setId("n1");
		note.setPriority(2);
		note.setCategory("myCat");
		StartGroupData startGroupData = new StartGroupData("g1");
		startGroupData.addNote(note);
		writer.writeStartGroup(startGroupData);
		
		Unit unit = new Unit("id");
		Segment seg = unit.appendSegment();
		seg.setSource("Source").appendCode("1", "[br/]");
		unit.addNote(new Note("Unit note1", AppliesTo.SOURCE));
		unit.addNote(new Note("Unit note2", AppliesTo.UNDEFINED));
		writer.writeUnit(unit);
		
		writer.writeEndGroup();

		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\">\n"
			+ " <file id=\"f1\">\n"
			+ "  <notes>\n"
			+ "   <note>File note</note>\n"
			+ "  </notes>\n"
			+ "  <group id=\"g1\">\n"
			+ "   <notes>\n"
			+ "    <note id=\"n1\" appliesTo=\"source\" priority=\"2\" category=\"myCat\">Group note</note>\n"
			+ "   </notes>\n"
			+ "   <unit id=\"id\">\n"
			+ "    <notes>\n"
			+ "     <note appliesTo=\"source\">Unit note1</note>\n"
			+ "     <note>Unit note2</note>\n"
			+ "    </notes>\n"
			+ "    <originalData>\n"
			+ "     <data id=\"d1\">[br/]</data>\n"
			+ "    </originalData>\n"
			+ "    <segment>\n"
			+ "     <source>Source<ph id=\"1\" dataRef=\"d1\"/></source>\n"
			+ "    </segment>\n"
			+ "   </unit>\n"
			+ "  </group>\n"
			+ " </file>\n"
			+ "</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}
	
	@Test
	public void testChangeTrack(){
		
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en");
		writer.setLineBreak("\n");
		writer.setUseIndentation(true);
		writer.setWithOriginalData(true);
		
		Unit unit = new Unit("id");
		Segment seg = unit.appendSegment();
		seg.setSource("Source").appendCode("1", "[br/]");
		Note note = new Note("Unit note1", AppliesTo.SOURCE);
		note.setId("n1");
		unit.addNote(note);
		note = new Note("Unit note2", AppliesTo.UNDEFINED);
		note.setId("n2");
		unit.addNote(note);
		ChangeTrack chTrack = new ChangeTrack();
		Revisions revisions = new Revisions("note");
		revisions.setRef("n1");
		chTrack.add(revisions);
		Revision revision = new Revision();
		revision.setAuthor("system");
		revision.setDatetime("2015-10-21T09:00:00+00:00");
		revisions.add(revision);
		Item item = new Item("content");
		item.setText("old note");
		revision.add(item);
		unit.setChangeTrack(chTrack);
		
		writer.writeUnit(unit);
		writer.close();
		System.out.println(strWriter.toString());
		String expectedString = "<?xml version=\"1.0\"?>\n"
				+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\">\n"
				+ " <file id=\"f1\">\n"
				+ "  <unit id=\"id\">\n"
				+ "   <ctr:changeTrack xmlns:ctr=\"urn:oasis:names:tc:xliff:changetracking:2.0\">\n"
				+ "    <ctr:revisions appliesTo=\"note\" ref=\"n1\">\n"
				+ "     <ctr:revision author=\"system\" datetime=\"2015-10-21T09:00:00+00:00\">\n"
				+ "      <ctr:item property=\"content\">old note</ctr:item>\n"
				+ "     </ctr:revision>\n"
				+ "    </ctr:revisions>\n"
				+ "   </ctr:changeTrack>\n"
				+ "   <notes>\n"
				+ "    <note id=\"n1\" appliesTo=\"source\">Unit note1</note>\n"
				+ "    <note id=\"n2\">Unit note2</note>\n"
				+ "   </notes>\n"
				+ "   <originalData>\n"
				+ "    <data id=\"d1\">[br/]</data>\n"
				+ "   </originalData>\n"
				+ "   <segment>\n"
				+ "    <source>Source<ph id=\"1\" dataRef=\"d1\"/></source>\n"
				+ "   </segment>\n"
				+ "  </unit>\n"
				+ " </file>\n"
				+ "</xliff>\n";
		assertEquals(expectedString, strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

//	@Test
//	public void testUnitWithcustomProperties () {
//		XLIFFWriter writer = new XLIFFWriter();
//		StringWriter strWriter = new StringWriter();
//		writer.create(strWriter, "en");
//		writer.setLineBreak("\n");
//		writer.setInlineStyle(OriginalDataStyle.OUTSIDE);
//		
//		Unit unit = new Unit("id");
//		unit.getCustomProperties().put("up1", "upv1");
//		unit.getCustomProperties().put("up2", "upv2");
//		ISegment seg = unit.appendNewSegment();
//		seg.setSource("Source.");
//		seg.getCustomProperties().put("sp1", "spv1");
//		seg.getCustomProperties().put("sp2", "spv2");
//		IPart ign = unit.appendNewIgnorable();
//		ign.setSource(" ign");
//		ign.getCustomProperties().put("ip1", "ipv1");
//		ign.getCustomProperties().put("ip2", "ipv2");
//		
//		writer.writeUnit(unit);
//		writer.close();
//
//		assertEquals("<?xml version=\"1.0\"?>\n"
//			+ STARTDOC+"srcLang=\"en\">\n"
//			+ "<file id=\"f1\">\n"
//			+ "<unit id=\"id\">\n"
//			+ "<segment>\n"
//			+ "<source>Source.</source>\n"
//			+ "<p:metadata xmlns:p=\"urn:oasis:names:tc:xliff:metadata:2.0\">\n"
//			+ "<p:meta type=\"sp1\">spv1</p:meta>\n"
//			+ "<p:meta type=\"sp2\">spv2</p:meta>\n"
//			+ "</p:metadata>\n"
//			+ "</segment>\n"
//			+ "<ignorable>\n"
//			+ "<source> ign</source>\n"
//			+ "<p:metadata xmlns:p=\"urn:oasis:names:tc:xliff:metadata:2.0\">\n"
//			+ "<p:meta type=\"ip1\">ipv1</p:meta>\n"
//			+ "<p:meta type=\"ip2\">ipv2</p:meta>\n"
//			+ "</p:metadata>\n"
//			+ "</ignorable>\n"
//			+ "<p:metadata xmlns:p=\"urn:oasis:names:tc:xliff:metadata:2.0\">\n"
//			+ "<p:meta type=\"up1\">upv1</p:meta>\n"
//			+ "<p:meta type=\"up2\">upv2</p:meta>\n"
//			+ "</p:metadata>\n"
//			+ "</unit>\n</file>\n</xliff>\n",
//			strWriter.toString());
//	}

	@Test
	public void testSegmentOutputWithCodesOutside () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en", "fr");
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		
		Unit unit = new Unit("id");
		createSegment(unit);
		writer.writeUnit(unit);
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<originalData>\n"
			+ "<data id=\"d1\">&lt;b></data>\n"
			+ "<data id=\"d2\">&lt;/b></data>\n"
			+ "<data id=\"d3\">&lt;/B></data>\n"
			+ "</originalData>\n"
			+ "<segment>\n"
			+ "<source><pc id=\"1\" dataRefEnd=\"d2\" dataRefStart=\"d1\">source</pc></source>\n"
			+ "<target><pc id=\"1\" dataRefEnd=\"d3\" dataRefStart=\"d1\">target</pc></target>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testSegmentOutputWithEmptyCodesOutside () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en");
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		
		Unit unit = new Unit("id");
		Segment seg = (Segment)unit.appendSegment();
		seg.getSource().append(TagType.STANDALONE, "ph1", "[br/]", false);
		seg.getSource().append(TagType.STANDALONE, "ph2", "", false);
		seg.getSource().append(TagType.STANDALONE, "ph3", null, false);
		writer.writeUnit(unit);
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<originalData>\n"
			+ "<data id=\"d1\">[br/]</data>\n"
			+ "</originalData>\n"
			+ "<segment>\n"
			+ "<source><ph id=\"ph1\" dataRef=\"d1\"/><ph id=\"ph2\"/><ph id=\"ph3\"/></source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testSegmentOutputWithoutCodes () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en", "fr");
		writer.setLineBreak("\n");
		writer.setWithOriginalData(false);
		
		Unit unit = new Unit("id");
		createSegment(unit);
		writer.writeUnit(unit);
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"1\">source</pc></source>\n"
			+ "<target><pc id=\"1\">target</pc></target>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

//	@Test
//	public void testGlossary () {
//		XLIFFWriter writer = new XLIFFWriter();
//		StringWriter strWriter = new StringWriter();
//		writer.create(strWriter, "en");
//		writer.setLineBreak("\n");
//		writer.setUseIndentation(true);
//		
//		Unit unit = new Unit("id");
//		unit.appendNewSegment().setSource("text");
//		IGlossEntry entry = new GlossEntry("term", "trans");
//		entry.setDefinition("def");
//		unit.getGlossaryEntries().add(entry);
//		writer.writeUnit(unit);
//		
//		writer.close();
//		assertEquals("<?xml version=\"1.0\"?>\n"
//			+ STARTDOC+"srcLang=\"en\">\n"
//			+ " <file id=\"f1\">\n"
//			+ "  <unit id=\"id\">\n"
//			+ "   <segment>\n"
//			+ "    <source>text</source>\n"
//			+ "   </segment>\n"
//			+ "   <g:glossary xmlns:g=\"urn:oasis:names:tc:xliff:glossary:2.0\">\n"
//			+ "    <g:glossEntry>\n"
//			+ "     <g:term>term</g:term>\n"
//			+ "     <g:translation>trans</g:translation>\n"
//			+ "     <g:definition>def</g:definition>\n"
//			+ "    </g:glossEntry>\n"
//			+ "   </g:glossary>\n"
//			+ "  </unit>\n"
//			+ " </file>\n"
//			+ "</xliff>\n",
//			strWriter.toString());
//	}

	@Test
	public void testSegmentOutputWithoutOriginalData () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en", "fr");
		writer.setLineBreak("\n");
		writer.setWithOriginalData(false);
		
		Unit unit = new Unit("id");
		createSegment(unit);
		writer.writeUnit(unit);
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"1\">source</pc></source>\n"
			+ "<target><pc id=\"1\">target</pc></target>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testExtensionElements () {
		Unit unit = new Unit("id");
		Segment seg = unit.appendSegment();
		seg.setSource("Source.");
		
		// Add an extension element
		unit.setExtElements(new ExtElements())
			.add(new ExtElement(new QName("myNamespaceURI", "myElement", "x1")))
				.addChild(new ExtContent("The content of the extension element."));

		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en");
		writer.setLineBreak("\n");
		writer.writeUnit(unit);
		writer.close();
		
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<x1:myElement xmlns:x1=\"myNamespaceURI\">The content of the extension element.</x1:myElement>\n"
			+ "<segment>\n"
			+ "<source>Source.</source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}
	
	@Test
	public void testSegmentOrder () {
		Unit unit = new Unit("id");
		// Source = "Source A. Source B."
		// Target = "Target B. Target A."
		Segment seg = unit.appendSegment();
		seg.setSource("Source A.");
		seg.setTarget("Target A.");
		seg.setTargetOrder(3);
		unit.appendIgnorable().setSource(" ");
		seg = unit.appendSegment();
		seg.setSource("Source B.");
		seg.setTarget("Target B");
		seg.setTargetOrder(1);
		
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "en", "fr");
		writer.setLineBreak("\n");
		writer.writeUnit(unit);
		writer.close();
		
		assertEquals("<?xml version=\"1.0\"?>\n"
			+ STARTDOC+"srcLang=\"en\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<unit id=\"id\">\n"
			+ "<segment>\n"
			+ "<source>Source A.</source>\n"
			+ "<target order=\"3\">Target A.</target>\n"
			+ "</segment>\n"
			+ "<ignorable>\n"
			+ "<source> </source>\n"
			+ "</ignorable>\n"
			+ "<segment>\n"
			+ "<source>Source B.</source>\n"
			+ "<target order=\"1\">Target B</target>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testInvalidCharacters () {
		XLIFFWriter writer = new XLIFFWriter();
		StringWriter strWriter = new StringWriter();
		writer.setLineBreak("\n");
		writer.setWithOriginalData(true);
		writer.create(strWriter, "en");

		Unit unit = new Unit("1");
		unit.appendIgnorable().getSource().append("\u0019\uFFFE");
		Fragment frag = unit.appendSegment().getSource();
		frag.append("\u0019\uFFFE");
		frag.append(TagType.STANDALONE, "1", "\u0019\uFFFE", true);
		writer.writeUnit(unit);
		
		writer.close();
		assertEquals("<?xml version=\"1.0\"?>\n<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\">\n"
			+ "<file id=\"f1\">\n<unit id=\"1\">\n"
			+ "<originalData>\n"
			+ "<data id=\"d1\"><cp hex=\"0019\"/><cp hex=\"FFFE\"/></data>\n"
			+ "</originalData>\n"
			+ "<ignorable>\n"
			+ "<source><cp hex=\"0019\"/><cp hex=\"FFFE\"/></source>\n"
			+ "</ignorable>\n"
			+ "<segment>\n"
			+ "<source><cp hex=\"0019\"/><cp hex=\"FFFE\"/><ph id=\"1\" dataRef=\"d1\"/></source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n"
			+ "</xliff>\n",
			strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}

	@Test
	public void testRewrite () {
		String input = "<?xml version='1.0'?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\" trgLang=\"fr\">\n"
			+ "<file translate=\"no\" id=\"fid1\" original=\"ori\">\n"
			+ "<unit id=\"id\" canResegment=\"no\">\n<segment>\n<source>Source 1.</source>\n<target>Target 1.</target>\n"
			+ "</segment>\n<segment>\n<source>Source 2.</source>\n<target>Target 2.</target>\n</segment>\n</unit>\n</file>\n</xliff>";
		XLIFFReader reader = new XLIFFReader(XLIFFReader.VALIDATION_MAXIMAL);
		XLIFFWriter writer = new XLIFFWriter();
		reader.open(input);
		StringWriter strWriter = new StringWriter();
		writer.create(strWriter, "fr");
		writer.setLineBreak("\n"); //TODO: need to detect automatically
		while ( reader.hasNext() ) {
			writer.writeEvent(reader.next());
		}
		writer.close();
		reader.close();

		String expected = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en\" trgLang=\"fr\">\n"
			+ "<file id=\"fid1\" translate=\"no\" original=\"ori\">\n"
			+ "<unit id=\"id\" canResegment=\"no\">\n"
			+ "<segment>\n"
			+ "<source>Source 1.</source>\n"
			+ "<target>Target 1.</target>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source>Source 2.</source>\n"
			+ "<target>Target 2.</target>\n"
			+ "</segment>\n</unit>\n</file>\n</xliff>\n";
		assertEquals(expected, strWriter.toString());
		XLIFFReader.validate(strWriter.toString(), null);
	}
	
	private void createSegment (Unit unit) {
		Segment seg = unit.appendSegment();
		seg.getSource().append(TagType.OPENING, "1", "<b>", false);
		seg.getSource().append("source");
		seg.getSource().append(TagType.CLOSING, "1", "</b>", false);
		Fragment frag = seg.getTarget(GetTarget.CREATE_EMPTY);
		frag.append(TagType.OPENING, "1", "<b>", false);
		frag.append("target");
		frag.append(TagType.CLOSING, "1", "</B>", false);
	}
	
//	private Candidate createAlternate (String prefix, float value) {
//		Candidate alt = new Candidate();
//		alt.setSimilarity(value);
//		alt.setType("tm");
//		alt.setOrigin("ori");
//		Fragment frag = new Fragment(alt.getDataStore());
//		frag.append(prefix+"-text");
//		frag.appendPlaceholder("1", "<br/>");
//		alt.setSource(frag);
//		
//		frag = new Fragment(alt.getDataStore());
//		frag.append(prefix.toUpperCase()+"-TEXT");
//		frag.appendPlaceholder("1", "<br/>");
//		alt.setTarget(frag);
//		
//		return alt;
//	}
	
}
