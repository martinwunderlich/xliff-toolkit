package net.sf.okapi.lib.xliff2.validation;

import static org.junit.Assert.*;

import java.text.Normalizer;
import java.text.Normalizer.Form;

import net.sf.okapi.lib.xliff2.validation.Rule.Normalization;
import net.sf.okapi.lib.xliff2.validation.Rule.Type;

import org.junit.Test;

public class RuleTest {
	
	@Test
	public void testSimple () {
		Rule r = new Rule("startsWith", "a");
		r.prepare();
		assertTrue(r.getType()==Type.STARTSWITH);
		assertEquals("a", r.getData());
		assertTrue(r.isCaseSensitive());
		assertTrue(r.isEnable());
		assertFalse(r.getExistsInSource());
		assertEquals(Normalization.NFC, r.getNormalization());
		assertEquals("a", r.getEffectiveData());
		assertFalse(r.isInherited());
	}

	@Test
	public void testCopyConstructor () {
		Rule r = new Rule("startsWith", "b");
		r.setCaseSensitive(false);
		r.setEnable(false);
		r.setExistsInSource(true);
		r.setNormalization(Normalization.NFD);
		r.setOccurs(3);
		r.setInherited(true);
		r.prepare();
		Rule r2 = new Rule(r);
		assertFalse(r==r2);
		assertFalse(r2.isCaseSensitive());
		assertFalse(r2.isEnable());
		assertTrue(r2.getExistsInSource());
		assertEquals(Normalization.NFD, r2.getNormalization());
		assertEquals(3, r2.getOccurs());
		assertEquals("b", r2.getEffectiveData());
		assertEquals("b", r2.getData());
		assertTrue(r2.isInherited());
	}

	@Test
	public void testNormalizationNFCCaseSensitive () {
		String original = "\u00C4\uFB03n";
		String expected = Normalizer.normalize(original, Form.NFC);
		Rule r = new Rule("isPresent", original);
		r.prepare();
		assertEquals(expected, r.getEffectiveData());
	}

	@Test
	public void testNormalizationNFDCaseSensitive () {
		String original = "\u00C4\uFB03n";
		String expected = Normalizer.normalize(original, Form.NFD);
		Rule r = new Rule("isPresent", original);
		r.setNormalization(Normalization.NFD);
		r.prepare();
		assertEquals(expected, r.getEffectiveData());
	}

	@Test
	public void testNormalizationNoneCaseSensitive () {
		String original = "\u00C4\uFB03n";
		String expected = original;
		Rule r = new Rule("isPresent", original);
		r.setNormalization(Normalization.NONE);
		r.prepare();
		assertEquals(expected, r.getEffectiveData());
	}

	@Test
	public void testNormalizationNFCNotCaseSensitive () {
		String original = "\u00C4\uFB03n";
		String expected = Normalizer.normalize(original.toLowerCase(), Form.NFC);
		Rule r = new Rule("isPresent", original);
		r.setCaseSensitive(false);
		r.prepare();
		assertEquals(expected, r.getEffectiveData());
	}

	@Test
	public void testNormalizationNFDNotCaseSensitive () {
		String original = "\u00C4\uFB03n";
		String expected = Normalizer.normalize(original.toLowerCase(), Form.NFD);
		Rule r = new Rule("isPresent", original);
		r.setCaseSensitive(false);
		r.setNormalization(Normalization.NFD);
		r.prepare();
		assertEquals(expected, r.getEffectiveData());
	}

	@Test
	public void testNormalizationNoneNotCaseSensitive () {
		String original = "\u00C4\uFB03n";
		String expected = original.toLowerCase();
		Rule r = new Rule("isPresent", original);
		r.setCaseSensitive(false);
		r.setNormalization(Normalization.NONE);
		r.prepare();
		assertEquals(expected, r.getEffectiveData());
	}

}
