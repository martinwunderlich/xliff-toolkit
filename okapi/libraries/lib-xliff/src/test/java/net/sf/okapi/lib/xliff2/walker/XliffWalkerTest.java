/*===========================================================================
  Copyright (C) 2011-2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/
package net.sf.okapi.lib.xliff2.walker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.lib.xliff2.core.Segment;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.document.FileNode;
import net.sf.okapi.lib.xliff2.document.UnitNode;
import net.sf.okapi.lib.xliff2.document.XLIFFDocument;
import net.sf.okapi.lib.xliff2.reader.XLIFFReader;
import net.sf.okapi.lib.xliff2.test.U;
import net.sf.okapi.lib.xliff2.walker.test.TestConstants;
import net.sf.okapi.lib.xliff2.walker.test.TestFileVisitor;
import net.sf.okapi.lib.xliff2.walker.test.TestModifyingSegmentVisitor;
import net.sf.okapi.lib.xliff2.walker.test.TestSegmentVisitor;
import net.sf.okapi.lib.xliff2.walker.test.TestUnitVisitor;

import org.junit.Before;
import org.junit.Test;

public class XliffWalkerTest {

	private XLIFFDocument doc;
	private XliffWalker testWalker;
	
	private final String root = U.getParentDir(this, "/example.xlf");
		
	@Before
	public void setup() {
		File xlfFile = new File(root + "/valid/everything-core-walker.xlf");
		
		XLIFFDocument doc = new XLIFFDocument();
		doc.load(xlfFile, XLIFFReader.VALIDATION_MAXIMAL);
		
		assertNotNull(doc);
		
		this.testWalker = new XliffWalker(doc);
	}
	
	@Test
	public void testDoWalk() {
		// Simple count test
		TestFileVisitor fileVisitor = new TestFileVisitor();
		int fileID = this.testWalker.addFileVisitor(fileVisitor);
		
		TestUnitVisitor unitVisitor = new TestUnitVisitor();
		int nodeID = this.testWalker.addUnitVisitor(unitVisitor);
		
		TestSegmentVisitor segmentVisitor = new TestSegmentVisitor();
		int segID = this.testWalker.addSegmentVisitor(segmentVisitor);
		
		this.testWalker.doWalk();
		
		assertEquals(1, fileVisitor.getCount());
		assertEquals(4, unitVisitor.getCount());
		assertEquals(5, segmentVisitor.getCount());
		
		// Segment manipulation test
		for(Unit unit : this.testWalker.getXlfDoc().getUnits()) {
			for(Segment segment : unit.getSegments()) {
				assertFalse(segment.getSource().isEmpty());
				assertFalse(segment.getTarget().isEmpty());
				assertNull(segment.getSubState());
			}
		}
		
		TestModifyingSegmentVisitor segmentVisitor2 = new TestModifyingSegmentVisitor();
		this.testWalker.removeAllVisitors();
		this.testWalker.addSegmentVisitor(segmentVisitor2);
		this.testWalker.doWalk();
		
		for(Unit unit : this.testWalker.getXlfDoc().getUnits()) {
			for(Segment segment : unit.getSegments()) {
				assertTrue(segment.getSource().isEmpty());
				assertTrue(segment.getTarget().isEmpty());
				assertEquals(TestConstants.SEGMENT_TEST_MARKER, segment.getSubState());
			}
		}
	}

	@Test
	public void testSetVisitors() {
		List<IXliffVisitor<FileNode>> fileVisitors = new ArrayList<IXliffVisitor<FileNode>>();
		fileVisitors.add(new TestFileVisitor());
		
		List<IXliffVisitor<UnitNode>> unitVisitors = new ArrayList<IXliffVisitor<UnitNode>>();
		unitVisitors.add(new TestUnitVisitor());
		unitVisitors.add(new TestUnitVisitor());
		
		this.testWalker.setVisitors(fileVisitors, unitVisitors, null /* segmentVisitors */);
		assertEquals(1, this.testWalker.getFileNodeVisitors().size());
		assertEquals(2, this.testWalker.getUnitNodeVisitors().size());
		
		this.testWalker.removeAllVisitors();
		assertEquals(0, this.testWalker.getFileNodeVisitors().size());
		assertEquals(0, this.testWalker.getUnitNodeVisitors().size());
	}

	@Test
	public void testAddFileVisitor() {
		IXliffVisitor<FileNode> fileVisitor = new TestFileVisitor();
		int ID = this.testWalker.addFileVisitor(fileVisitor);
		
		assertEquals(0, ID);
		
		IXliffVisitor<FileNode> fileVisitor2 = new TestFileVisitor();
		int ID2 = this.testWalker.addFileVisitor(fileVisitor2);
		
		assertEquals(1, ID2);
		
		assertEquals(2, this.testWalker.getFileNodeVisitors().size());
		this.testWalker.removeFileVisitor(ID);
		assertEquals(1, this.testWalker.getFileNodeVisitors().size());
	}

	@Test
	public void testAddUnitVisitor() {
		IXliffVisitor<UnitNode> unitVisitor = new TestUnitVisitor();
		int ID = this.testWalker.addUnitVisitor(unitVisitor);
		
		assertEquals(0, ID);
		
		IXliffVisitor<UnitNode> unitVisitor2 = new TestUnitVisitor();
		int ID2 = this.testWalker.addUnitVisitor(unitVisitor2);
		
		assertEquals(1, ID2);
		
		assertEquals(2, this.testWalker.getUnitNodeVisitors().size());
		this.testWalker.removeUnitVisitor(ID);
		assertEquals(1, this.testWalker.getUnitNodeVisitors().size());
	}

	@Test
	public void testAddSegmentVisitor() {
		IXliffVisitor<Segment> segmentVisitor = new TestSegmentVisitor();
		int ID = this.testWalker.addSegmentVisitor(segmentVisitor);
		
		assertEquals(0, ID);
		
		IXliffVisitor<Segment> segmentVisitor2 = new TestSegmentVisitor();
		int ID2 = this.testWalker.addSegmentVisitor(segmentVisitor2);
		
		assertEquals(1, ID2);
		
		assertEquals(2, this.testWalker.getSegmentVisitors().size());
		this.testWalker.removeSegmentVisitor(ID);
		assertEquals(1, this.testWalker.getSegmentVisitors().size());
	}
	
	@Test
	public void testIncorrectParams() {
		boolean exceptionThrown = false; 
		
		try { 
			this.testWalker.addFileVisitor(null);
		} catch (IllegalArgumentException ex) {
			exceptionThrown = true;
		}
		
		assertTrue(exceptionThrown);
		
		exceptionThrown = false; 
		try { 
			this.testWalker.addUnitVisitor(null);
		} catch (IllegalArgumentException ex) {
			exceptionThrown = true;
		}
		
		assertTrue(exceptionThrown);
		
		exceptionThrown = false; 
		try { 
			this.testWalker.addSegmentVisitor(null);
		} catch (IllegalArgumentException ex) {
			exceptionThrown = true;
		}
		
		assertTrue(exceptionThrown);
	}
}
