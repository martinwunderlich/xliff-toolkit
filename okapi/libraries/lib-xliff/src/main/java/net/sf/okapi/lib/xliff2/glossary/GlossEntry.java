/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.glossary;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.sf.okapi.lib.xliff2.core.ExtAttributes;
import net.sf.okapi.lib.xliff2.core.ExtElements;
import net.sf.okapi.lib.xliff2.core.IWithExtAttributes;
import net.sf.okapi.lib.xliff2.core.IWithExtElements;

/**
 * Represents the &lt;glossEntry> element of the 
 * <a href='http://docs.oasis-open.org/xliff/xliff-core/v2.0/xliff-core-v2.0.html#glossary-module'>Glossary module</a>.
 */
public class GlossEntry implements IWithExtAttributes, IWithExtElements, Iterable<Translation> {

	private String id;
	private String ref;
	private Term term;
	private Definition definition;
	private List<Translation> translations;
	private ExtElements xelems;
	private ExtAttributes xattrs;
	
	/**
	 * Creates a new {@link GlossEntry} object.
	 */
	public GlossEntry () {
		term = new Term((String)null);
		definition = new Definition((String)null);
		translations = new ArrayList<>(1);
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public GlossEntry (GlossEntry original) {
		this.id = original.id;
		this.ref = original.ref;
		this.term = new Term(original.term);
		this.definition = new Definition(original.definition);
		
		translations = new ArrayList<>(original.translations.size());
		for ( Translation trans : original.translations ) {
			translations.add(new Translation(trans));
		}
		if ( original.hasExtAttribute() ) {
			xattrs = new ExtAttributes(original.xattrs);
		}
		if ( original.hasExtElements() ) {
			xelems = new ExtElements(original.xelems);
		}
	}

	/**
	 * Gets the id for this entry.
	 * @return the id for this entry (can be null).
	 */
	public String getId () {
		return id;
	}

	/**
	 * Sets the id for this entry.
	 * @param id the new id for this entry (can be null).
	 */
	public void setId (String id) {
		this.id = id;
	}

	/**
	 * Gets the reference for this entry.
	 * @return the reference for this entry (can be null).
	 */
	public String getRef () {
		return ref;
	}

	/**
	 * Sets the reference for this entry.
	 * @param ref the new reference for this entry (can be null).
	 */
	public void setRef (String ref) {
		this.ref = ref;
	}

	/**
	 * Gets the term for this entry.
	 * @return the term for this entry.
	 */
	public Term getTerm () {
		return term;
	}

	/**
	 * Sets a new term for this entry.
	 * @param term the new term for this entry (must not be null).
	 */
	public void setTerm (Term term) {
		this.term = term;
	}

	/**
	 * Gets the definition for this entry.
	 * @return the definition for this entry (can be null).
	 */
	public Definition getDefinition () {
		return definition;
	}

	/**
	 * Sets a new definition for this entry.
	 * @param definition the new definition for this entry (can be null).
	 */
	public void setDefinition (Definition definition) {
		this.definition = definition;
	}

	/**
	 * Adds a translation to this entry.
	 * @param text the text of the translation to add.
	 */
	public void addTranslation (String text) {
		getTranslations().add(new Translation(text));
	}

	@Override
	public ExtElements getExtElements () {
		if ( xelems == null ) xelems = new ExtElements();
		return xelems;
	}

	@Override
	public boolean hasExtElements () {
		if ( xelems == null ) return false;
		return (xelems.size() > 0);
	}

	@Override
	public ExtElements setExtElements (ExtElements elements) {
		this.xelems = elements;
		return getExtElements();
	}

	@Override
	public void setExtAttributes (ExtAttributes attributes) {
		this.xattrs = attributes;
	}

	@Override
	public ExtAttributes getExtAttributes () {
		if ( xattrs == null ) {
			xattrs = new ExtAttributes();
		}
		return xattrs;
	}

	@Override
	public boolean hasExtAttribute () {
		if ( xattrs == null ) return false;
		return !xattrs.isEmpty();
	}

	@Override
	public String getExtAttributeValue (String namespaceURI,
		String localName)
	{
		if ( xattrs == null ) return null;
		return xattrs.getAttributeValue(namespaceURI, localName);
	}

	@Override
	public Iterator<Translation> iterator () {
		return translations.iterator();
	}

	/**
	 * Gets the list of the {@link Translation} objects for this entry.
	 * @return the list of the {@link Translation} objects for this entry (never null).
	 */
	public List<Translation> getTranslations () {
		return translations;
	}

}
