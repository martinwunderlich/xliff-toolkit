/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.Util;
import net.sf.okapi.lib.xliff2.its.ITSItems;
import net.sf.okapi.lib.xliff2.its.IWithITSAttributes;

/**
 * Represents common data for the opening and closing {@link MTag}.
 */
class MTagCommon implements IWithITSAttributes {

	private String id;
	private String type;
	private String value;
	private String ref;
	private Boolean translate;
	private ITSItems itsItems;

	public MTagCommon (String id,
		String type)
	{
		setId(id);
		setType(type);
	}
	
	/**
	 * Copy constructor.
	 * @param original the existing {@link MTagCommon} to duplicate.
	 */
	public MTagCommon (MTagCommon original) {
		// Copy the AMarker-specific fields
		id = original.id;
		type = original.type;
		value = original.value;
		ref = original.ref;
		translate = original.translate;
		if ( original.hasITSItem() ) {
			setITSItems(new ITSItems(original.itsItems));
		}
	}
	
	@Override
	public String getId () {
		return id;
	}

	public void setId (String id) {
		if ( id == null ) {
			throw new InvalidParameterException("ID cannot be null.");
		}
		this.id = id;
	}

	public void setType (String type) {
		if ( type == null ) {
			type = MTag.TYPE_DEFAULT; // Use the default
		}
		else {
			// Is it one of the standard values?
			if ( ";generic;comment;term;".indexOf(";"+type+";") == -1 ) {
				// If not: check the pattern "prefix:value"
				int n = type.indexOf(':');
				if (( n == -1 ) || ( n == 0 ) || ( n == type.length()-1 )) {
					throw new InvalidParameterException(String.format("Invalid value '%s' for an annotation type.", type));
				}
			}
		}
		this.type = type;
	}

	public String getRef () {
		return ref;
	}
	
	public void setRef (String ref) {
		this.ref = ref;
	}

	public String getValue () {
		return value;
	}
	
	public void setValue (String value) {
		this.value = value;
	}

	public Boolean getTranslate () {
		return translate;
	}

	public void setTranslate (Boolean translate) {
		this.translate = translate;
	}

	/**
	 * Indicates if this object is equals to a given one.
	 * @param other the other object to compare.
	 * @return true if the two objects are identical.
	 */
	public boolean equals (MTagCommon other) {
		if ( other == null ) return false;
		if ( this == other ) return true;
		if ( Util.compareAllowingNull(type, other.getType()) != 0 ) return false;
		if ( Util.compareAllowingNull(id, other.getId()) != 0 ) return false;
		if ( Util.compareAllowingNull(ref, other.getRef()) != 0 ) return false;
		if ( Util.compareAllowingNull(value, other.getValue()) != 0 ) return false;
		// Note that translate can be null, and it's OK
		if ( translate != other.getTranslate() ) return false;
		//TODO: compare the ITS items
		return true;
	}

	@Override
	public boolean hasITSItem () {
		if ( itsItems == null ) return false;
		return !itsItems.isEmpty();
	}

	@Override
	public ITSItems getITSItems () {
		if ( itsItems == null ) {
			itsItems = new ITSItems();
		}
		return itsItems;
	}

	@Override
	public void setITSItems (ITSItems itsItems) {
		this.itsItems = itsItems;
	}

	public String getType () {
		return type;
	}

}
