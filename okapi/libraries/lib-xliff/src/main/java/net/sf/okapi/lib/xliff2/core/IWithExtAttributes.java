/*===========================================================================
  Copyright (C) 2011-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Provides a common interface to all objects that can have extension attributes.
 * @see ExtAttributes
 * @see ExtAttribute
 */
public interface IWithExtAttributes {

	/**
	 * Gets the {@link ExtAttributes} object for the parent object.
	 * If none exists, one is created.
	 * @return the {@link ExtAttributes} object for the parent object, never null.
	 */
	public ExtAttributes getExtAttributes ();
	
	/**
	 * Sets the {@link ExtAttributes} object associated with this object.
	 * @param attributes the {@link ExtAttributes} object associated with this object.
	 * If null, a new {@link ExtAttributes} object is created.
	 */
	public void setExtAttributes (ExtAttributes attributes);

	/**
	 * Indicates if at least one extension attribute is present.
	 * @return true if at least one extension attribute is present; false otherwise.
	 */
	public boolean hasExtAttribute ();

	/**
	 * Gets the value for a given extension attribute.
	 * @param namespaceURI the URI of the namespace for the attribute.
	 * @param localName the name of the attribute.
	 * @return the value of the extension attribute, or null if it does not exist.
	 */
	public String getExtAttributeValue (String namespaceURI,
		String localName);

}
