/*===========================================================================
  Copyright (C) 2011-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the information associated with the skeleton.
 */
public class Skeleton {
	
	private String href;
	private ArrayList<IExtChild> children;

	/**
	 * Sets the href for this skeleton.
	 * @param href the new href to set (can be null).
	 */
	public void setHref (String href) {
		this.href = href;
	}
	
	/**
	 * Gets the href for this skeleton.
	 * @return the href for this skeleton (can be null).
	 */
	public String getHref () {
		return href;
	}

	/**
	 * Adds an extension child to this skeleton.
	 * @param child the extension child object to add.
	 */
	public void addChild (IExtChild child) {
		if ( children == null ) children = new ArrayList<>(2);
		children.add(child);
	}

	/**
	 * Gets the list of extension children for this skeleton.
	 * @return the list of extension children for this skeleton (can be null).
	 */
	public List<IExtChild> getChildren () {
		return children;
	}
	
}
