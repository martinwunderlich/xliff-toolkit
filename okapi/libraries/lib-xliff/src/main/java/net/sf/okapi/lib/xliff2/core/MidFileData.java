/*===========================================================================
  Copyright (C) 2011-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.changeTracking.ChangeTrack;
import net.sf.okapi.lib.xliff2.metadata.Metadata;
import net.sf.okapi.lib.xliff2.validation.Validation;

/**
 * Implements the {@link IWithExtElements} and {@link IWithNotes} interfaces.
 */
public class MidFileData extends DataWithExtElements implements IWithNotes, IWithMetadata, IWithValidation, IWithChangeTrack {

	private Metadata metadata;
	private Validation validation;
	private ChangeTrack changeTrack;
	private Notes notes;

	@Override
	public void addNote (Note note) {
		if ( notes == null ) notes = new Notes();
		notes.add(note);
	}

	@Override
	public Notes getNotes () {
		if ( notes == null ) notes = new Notes();
		return notes;
	}
	
	@Override
	public int getNoteCount () {
		if ( notes == null ) return 0;
		return notes.size();
	}

	@Override
	public boolean hasMetadata () {
		if ( metadata == null ) return false;
		return !metadata.isEmpty();
	}

	@Override
	public Metadata getMetadata () {
		if ( metadata == null ) metadata = new Metadata();
		return metadata;
	}

	@Override
	public void setMetadata (Metadata metadata) {
		this.metadata = metadata;
	}

	@Override
	public boolean hasValidation () {
		if ( validation == null ) return false;
		return !validation.isEmpty();
	}

	@Override
	public Validation getValidation () {
		if ( validation == null ) validation = new Validation();
		return validation;
	}

	@Override
	public void setValidation (Validation validation) {
		this.validation = validation;
	}

	@Override
	public ChangeTrack getChangeTrack () {
		if ( changeTrack == null ) changeTrack = new ChangeTrack();
		return changeTrack;
	}

	@Override
	public void setChangeTrack (ChangeTrack changeTrack) {
		this.changeTrack = changeTrack;
	}

	@Override
	public boolean hasChangeTrack () {
		if ( changeTrack == null ) return false;
		return !changeTrack.isEmpty();
	}
	
}
