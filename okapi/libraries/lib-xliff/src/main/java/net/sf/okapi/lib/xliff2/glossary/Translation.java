/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.glossary;

/**
 * Represents the &lt;translation> element
 * <a href='http://docs.oasis-open.org/xliff/xliff-core/v2.0/xliff-core-v2.0.html#glossary-module'>Glossary module</a>.
 */
public class Translation extends BaseGlossaryField {

	private String id;
	private String ref;

	/**
	 * Creates a {@link Translation} object with a given text.
	 * @param text the text of the translation.
	 */
	public Translation (String text) {
		setText(text);
	}
	
	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public Translation (Translation original) {
		// Create the new object from its base class copy constructor
		super(original);
		// Duplicate additional fields
		this.id = original.id;
		this.ref = original.ref;
	}
	
	/**
	 * Gets the id for this translation.
	 * @return the id for this translation (can be null).
	 */
	public String getId () {
		return id;
	}

	/**
	 * Sets the id for this translation.
	 * @param id the new id for this translation (can be null).
	 */
	public void setId (String id) {
		this.id = id;
	}

	/**
	 * Gets the reference for this translation.
	 * @return the reference for this translation (can be null).
	 */
	public String getRef () {
		return ref;
	}

	/**
	 * Sets the reference for this translation.
	 * @param ref the new reference for this translation.
	 */
	public void setRef (String ref) {
		this.ref = ref;
	}

}
