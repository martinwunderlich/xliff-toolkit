/*===========================================================================
  Copyright (C) 2012-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Provides a common interface to all objects that can have extension elements.
 * @see ExtElements
 * @see ExtElement
 */
public interface IWithExtElements {

	/**
	 * Gets the {@link ExtElements} object associated with this object.
	 * If none exists one is created.
	 * @return the {@link ExtElements} object associated with this object. If none exists one is created.
	 */
	public ExtElements getExtElements ();
	
	/**
	 * Sets the {@link ExtElements} object associated with this object.
	 * @param elements the {@link ExtElements} object associated with this object.
	 * If null, a new {@link ExtElements} object is created.
	 * @return the {@link ExtElements} object associated with this object, never null.
	 */
	public ExtElements setExtElements (ExtElements elements);

	/**
	 * Indicates if there is a {@link ExtElements} object associated with this object.
	 * @return true if there is a {@link ExtElements} object associated with this object,
	 * false otherwise.
	 */
	public boolean hasExtElements ();

}
