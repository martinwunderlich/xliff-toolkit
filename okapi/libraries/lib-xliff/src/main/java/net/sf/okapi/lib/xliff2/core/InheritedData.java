/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.core.Directionality;
import net.sf.okapi.lib.xliff2.its.AnnotatorsRef;

/**
 * Implements the {@link IWithInheritedData} interface.
 */
public class InheritedData implements IWithInheritedData {

	private boolean translate = true;
	private boolean canResegment = true;
	private Directionality srcDir = Directionality.AUTO;
	private Directionality trgDir = Directionality.AUTO;
	private AnnotatorsRef annotators = null;

	/**
	 * Creates a InheritedData object with default values.
	 */
	public InheritedData () {
		// Defaults
	}
	
	/**
	 * Creates a new InheritedData object from an existing one.
	 * @param original the InheritedData object to get the data from.
	 */
	public InheritedData (InheritedData original) {
		setInheritableData(original);
	}
	
	/**
	 * Sets all the data from a given object.
	 * @param original the object from which to get the data.
	 */
	public void setInheritableData (IWithInheritedData original) {
		translate = original.getTranslate();
		canResegment = original.getCanResegment();
		srcDir = original.getSourceDir();
		trgDir = original.getTargetDir();
		if ( original.getAnnotatorsRef() != null ) {
			annotators = new AnnotatorsRef(original.getAnnotatorsRef());
		}
	}

	@Override
	public boolean getCanResegment () {
		return canResegment;
	}
	
	@Override
	public void setCanResegment (boolean canResegment) {
		this.canResegment = canResegment;
	}
	
	@Override
	public boolean getTranslate () {
		return translate;
	}
	
	@Override
	public void setTranslate (boolean translate) {
		this.translate = translate;
	}
	
	@Override
	public Directionality getSourceDir () {
		return srcDir;
	}

	@Override
	public void setSourceDir (Directionality dir) {
		this.srcDir = dir;
	}

	@Override
	public Directionality getTargetDir () {
		return trgDir;
	}

	@Override
	public void setTargetDir (Directionality dir) {
		this.trgDir = dir;
	}

	@Override
	public AnnotatorsRef getAnnotatorsRef () {
		return annotators;
	}

	@Override
	public void setAnnotatorsRef (AnnotatorsRef annotators) {
		this.annotators = annotators;
	}

}
