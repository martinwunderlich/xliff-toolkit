/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.renderer;

import net.sf.okapi.lib.xliff2.core.CTag;
import net.sf.okapi.lib.xliff2.core.MTag;

/*
 * Represents an object among those composing a fragment. 
 */
public interface IFragmentObject {

	/**
	 * Generates the output for a given format.
	 * @return the string representation of this inline object.
	 */
	public String render ();
	
	/**
	 * Gets the text of this object, if the object is a String.
	 * @return the text.
	 * @throws ClassCastException if the object is not a String.
	 */
	public String getText ();
	
	/**
	 * Gets the {@link CTag} of this object, if the object is of that type
	 * @return the CTag of this object.
	 * @throws ClassCastException if the object is not a CTag.
	 */
	public CTag getCTag ();
	
	/**
	 * Gets the {@link MTag} of this object, if the object is of that type
	 * @return the MTag of this object.
	 * @throws ClassCastException if the object is not a MTag.
	 */
	public MTag getMTag ();

	/**
	 * Gets the original object.
	 * @return the original object.
	 */
	public Object getObject ();

}
