/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.document;

import java.util.ArrayList;

import net.sf.okapi.lib.xliff2.core.InsingnificantPartData;

abstract class BeforeAndAfter {

	private ArrayList<InsingnificantPartData> before;
	private ArrayList<InsingnificantPartData> after;

	public void add (InsingnificantPartData data,
		boolean placeBefore)
	{
		if ( placeBefore ) {
			if ( before == null ) {
				before = new ArrayList<InsingnificantPartData>(); 
			}
			before.add(data);
		}
		else {
			if ( after == null ) {
				after = new ArrayList<InsingnificantPartData>();
			}
			after.add(data);
		}
	}
	
}
