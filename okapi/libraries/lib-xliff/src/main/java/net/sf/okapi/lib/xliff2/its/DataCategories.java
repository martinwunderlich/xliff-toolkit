/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.its;

/**
 * Represents the identifiers for the different
 * <a href='http://www.w3.org/TR/its20/#datacategories-overview'>types of ITS data categories</a>.
 */
public class DataCategories {

	public static final String LOCQUALITYISSUE = "localization-quality-issue";
	public static final String PROVENANCE = "provenance";
	public static final String DOMAIN = "domain";
	public static final String TEXTANALYSIS = "text-analysis";
	public static final String MTCONFIDENCE = "mt-confidence";
	
	public static final String TERMINOLOGY = "terminology";
	
	public static final String TRANSLATE = "translate";
	
	public static final String LIST = ";translate;localization-note;terminology;directionality;language-information;"
		+ "elements-within-text;domain;text-analysis;locale-filter;provenance;external-resource;target-pointer;"
		+ "id-value;preserve-space;localization-quality-issue;localization-quality-rating;mt-confidence;"
		+ "allowed-characters;storage-size;";
}
