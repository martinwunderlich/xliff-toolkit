/*===========================================================================
  Copyright (C) 2012-2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

/**
 * Represents the information associated with an insignificant part
 * (from the XLIFF viewpoint).
 */
public class InsingnificantPartData {

	/**
	 * Types of insignificant parts.
	 */
	public static enum InsignificantPartType {
		/**
		 * Normal text content (e.g indentation, etc).
		 */
		TEXT,
		/**
		 * XML Comment.
		 */
		COMMENT,
		/**
		 * XML processing Instruction.
		 */
		PI
	};
	
	private String data;
	private InsignificantPartType type;
	
	/**
	 * Creates a new {@link InsingnificantPartData} object.
	 * @param type the type of the object.
	 * @param data the data for the new part.
	 */
	public InsingnificantPartData (InsignificantPartType type,
		String data)
	{
		this.type = type;
		this.data = data;
	}

	/**
	 * Gets the data for this {@link InsingnificantPartData} object.
	 * @return the data for this {@link InsingnificantPartData} object.
	 */
	public String getData () {
		return data;
	}
	
	/**
	 * Gets the type for this {@link InsingnificantPartData} object.
	 * @return the type for this {@link InsingnificantPartData} object.
	 */
	public InsignificantPartType getType () {
		return type;
	}

}
