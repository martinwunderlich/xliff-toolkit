/*===========================================================================
  Copyright (C) 2011-2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/
package net.sf.okapi.lib.xliff2.walker;

import net.sf.okapi.lib.xliff2.document.FileNode;
import net.sf.okapi.lib.xliff2.document.UnitNode;

/**
 * Provides the context for visiting the 
 * visitee during traversal of the XLIFF tree.
 * 
 * The context consists of file, unit, and segment index, 
 * of which only the file is required. The other may be null.  
 * 
 * @author Martin Wunderlich
 *
 */
public class VisitationContext {

	private Integer segmentIndex;
	private FileNode fileNode;
	private UnitNode unitNode;


	public VisitationContext(FileNode fileNode) {
		this.fileNode = fileNode;		
	}

	public VisitationContext(FileNode fileNode, UnitNode unitNode) {
		this.fileNode = fileNode;
		this.unitNode = unitNode;
	}

	public VisitationContext(FileNode fileNode, UnitNode unitNode, int segmentIndex) {
		this.fileNode = fileNode;
		this.unitNode = unitNode;
		this.segmentIndex = segmentIndex;
	}

	public FileNode getFile() {
		return fileNode;
	}

	public UnitNode getUnit() {
		return unitNode;
	}

	public Integer getSegmentIndex() {
		return segmentIndex;
	}
}
