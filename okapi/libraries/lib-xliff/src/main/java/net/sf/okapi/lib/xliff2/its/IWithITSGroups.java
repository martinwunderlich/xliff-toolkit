/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.its;

import java.util.List;

/**
 * Provides a common interface to all objects that can have ITS stand-off elements (groups)
 */
public interface IWithITSGroups {

	/**
	 * Indicates if the object has at least one ITS group.
	 * @return true if the object has at least one ITS group.
	 */
	public boolean hasITSGroup ();
	
	/**
	 * Gets the ITS groups associated with this object.
	 * @return the ITS groups associated with this object (can be empty, but never null).
	 */
	public List<DataCategoryGroup<?>> getITSGroups ();
	
	/**
	 * Adds an ITS group to this object.
	 * @param group the group to add.
	 */
	public DataCategoryGroup<?> addITSGroup (DataCategoryGroup<?> group);

}
