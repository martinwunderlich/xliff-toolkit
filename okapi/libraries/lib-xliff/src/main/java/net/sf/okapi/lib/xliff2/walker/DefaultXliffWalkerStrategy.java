/*===========================================================================
  Copyright (C) 2011-2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/
package net.sf.okapi.lib.xliff2.walker;

import net.sf.okapi.lib.xliff2.core.Segment;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.document.FileNode;
import net.sf.okapi.lib.xliff2.document.UnitNode;

public class DefaultXliffWalkerStrategy extends AbstractXliffWalkerStrategy {

	public DefaultXliffWalkerStrategy(XliffWalker walker) {
		super(walker);
	}

	@Override
	public void doWalk() {
		for (String fileNodeID : walker.getFileNodeIds()) {
            FileNode fileNode = walker.getFileNode(fileNodeID);
            
            for(IXliffVisitor<FileNode> visitor : walker.getFileNodeVisitors())
            	visitor.visit(fileNode, new VisitationContext(fileNode));
            
            for (UnitNode unitNode : fileNode.getUnitNodes()) {
            	Unit unit = unitNode.get();
            	
            	for(IXliffVisitor<UnitNode> visitor : walker.getUnitNodeVisitors())
                	visitor.visit(unitNode, new VisitationContext(fileNode, unitNode));
            	
            	int segmentIndex = 0;
            	for (Segment segment : unit.getSegments()) {
            		for(IXliffVisitor<Segment> visitor : walker.getSegmentVisitors())
                    	visitor.visit(segment, new VisitationContext(fileNode, unitNode, segmentIndex));
            		
                	segmentIndex++;
                }
            }
		}
	}
}
