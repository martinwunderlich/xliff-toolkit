/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.renderer;

import java.util.Stack;

import net.sf.okapi.lib.xliff2.NSContext;
import net.sf.okapi.lib.xliff2.core.Fragment;

/**
 * Provides an iterable interface for the objects making up a fragment.
 * Each object return by the iterator implements {@link IFragmentObject} providing both: 
 * access to the underlying object, and a string representation of that object. 
 */
public interface IFragmentRenderer extends Iterable<IFragmentObject> {

	/**
	 * Sets the fragment to iterate through and the namespace context.
	 * @param fragment the fragment to iterate through.
	 * @param nsStack the namespace context (can be null).
	 */
	public void set (Fragment fragment,
		Stack<NSContext> nsStack);

}
