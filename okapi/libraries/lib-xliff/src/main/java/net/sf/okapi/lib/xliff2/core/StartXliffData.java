/*===========================================================================
  Copyright (C) 2011-2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.InvalidParameterException;
import net.sf.okapi.lib.xliff2.Util;

/**
 * Represents the information associated with an &lt;xliff> element. 
 */
public class StartXliffData extends DataWithExtAttributes {
	
	private String version;
	private String sourceLang;
	private String targetLang;

	/**
	 * Creates a {@link StartXliffData} object.
	 * @param version the version of the XLIFF document, use null to get the default.
	 */
	public StartXliffData (String version) {
		if ( version == null ) version = "2.0";
		this.version = version;
	}
	
	/**
	 * Gets the version of this document.
	 * @return the version of this document.
	 */
	public String getVersion () {
		return version;
	}

	/**
	 * Gets the language code of the source for this document.
	 * @return the language code for the source.
	 */
	public String getSourceLanguage () {
		return sourceLang;
	}
	
	/**
	 * Sets the source language of the document.
	 * @param sourceLang the source language to set (must not be null).
	 * @throws InvalidParameterException if the language code is invalid.
	 */
	public void setSourceLanguage (String sourceLang) {
		String msg;
		if ( (msg = Util.validateLang(sourceLang)) != null ) {
			throw new InvalidParameterException(String.format("The source language value '%s' is invalid.\n"+msg, sourceLang));
		}
		this.sourceLang = sourceLang;
	}
	
	/**
	 * Gets the target language of the document.
	 * @return the target language of the document (can be null).
	 */
	public String getTargetLanguage () {
		return targetLang;
	}

	/**
	 * Sets the target language for this document.
	 * @param targetLang the target language to set (can be null).
	 * @throws InvalidParameterException if the language code is invalid.
	 */
	public void setTargetLanguage (String targetLang) {
		// Allow null for the target
		String msg;
		if (( targetLang != null ) && ((msg = Util.validateLang(targetLang)) != null) ) {
			throw new InvalidParameterException(String.format("The target language value '%s' is invalid.\n"+msg, targetLang));
		}
		this.targetLang = targetLang;
	}

	/**
	 * Sets a namespace declaration for this document.
	 * @param prefix the prefix to use for the namespace.
	 * @param namespaceURI the namespace URI.
	 */
	public void setNamespace (String prefix,
		String namespaceURI)
	{
		getExtAttributes().setNamespace(prefix, namespaceURI);
	}

}
