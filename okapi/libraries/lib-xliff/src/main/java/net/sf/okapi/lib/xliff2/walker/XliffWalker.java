/*===========================================================================
  Copyright (C) 2011-2016 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.walker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.sf.okapi.lib.xliff2.core.Segment;
import net.sf.okapi.lib.xliff2.document.FileNode;
import net.sf.okapi.lib.xliff2.document.UnitNode;
import net.sf.okapi.lib.xliff2.document.XLIFFDocument;

/**
 * Allows for traversal of the tree structure of an XLIFF document, 
 * in a fashion similar to visitor pattern.
 * 
 * Three types of visitor can be added to the XliffWalker: File visitors, 
 * Unit visitors, and Segment visitors. The exact order in which the visit()
 * method of these is called is determined by the XliffWalkerStrategy, for 
 * which a default implementation is provided. 
 * 
 * All visitors stored in the walked are stored in insertion order. This 
 * order is maintained during traversal using the default strategy.
 *  
 * @author Martin Wunderlich
 *
 */
public class XliffWalker {

	private XLIFFDocument xlf;
	private final AbstractXliffWalkerStrategy strategy;
	
	private Map<Integer, IXliffVisitor<FileNode>> fileVisitors = new TreeMap<>();
	private Map<Integer, IXliffVisitor<UnitNode>> unitVisitors = new TreeMap<>();
	private Map<Integer, IXliffVisitor<Segment>> segmentVisitors = new TreeMap<>();
	
	private int fileVisitorID = -1;
	private int unitVisitorID = -1;
	private int segmentVisitorID = -1;

	public XliffWalker(XLIFFDocument doc) {
		if (doc == null)
			throw new IllegalArgumentException("A valid XLIFF document must be provided.");
		
		this.xlf = doc;
		this.strategy = new DefaultXliffWalkerStrategy(this);
	}
	
	public XliffWalker(XLIFFDocument doc, AbstractXliffWalkerStrategy strategy) {
		if (doc == null)
			throw new IllegalArgumentException("A valid XLIFF document must be provided.");
		if (strategy == null)
			throw new IllegalArgumentException("A valid strategy object must be provided.");
		
		this.xlf = doc; 
		this.strategy = strategy;
	}
	
	/** 
	 * Main method for running the traversal. Should only be called after
	 * some visitors have been added to the Walker. Otherwise, an exception
	 * will be thrown.
	 */
	public void doWalk() {
		if(this.fileVisitors.isEmpty() && this.unitVisitors.isEmpty() && this.segmentVisitors.isEmpty())
			throw new IllegalStateException("At least one visitor must be added before running the xliff doc traversal");
		
		this.strategy.doWalk();
	}

	// Setters / Adders for visitors
	public void setVisitors(List<IXliffVisitor<FileNode>> fileVisitors, List<IXliffVisitor<UnitNode>> unitVisitors, List<IXliffVisitor<Segment>> segmentVisitors) {
		if (fileVisitors != null)
			for(IXliffVisitor<FileNode> fileVisitor : fileVisitors)
				this.addFileVisitor(fileVisitor);
		
		if (unitVisitors != null)
			for(IXliffVisitor<UnitNode> unitVisitor : unitVisitors)
				this.addUnitVisitor(unitVisitor);
		
		if (segmentVisitors != null)
			for(IXliffVisitor<Segment> segmentVisitor : segmentVisitors)
				this.addSegmentVisitor(segmentVisitor);
	}
	
	public Integer addFileVisitor(IXliffVisitor<FileNode> fileVisitor) {
		if (fileVisitor == null)
			throw new IllegalArgumentException("A valid file visitor must be provided.");
		
		Integer key = nextFileVisitorID();
		this.fileVisitors.put(key, fileVisitor);
		
		return key;
	}
	
	public Integer addUnitVisitor(IXliffVisitor<UnitNode> unitVisitor) {
		if (unitVisitor == null)
			throw new IllegalArgumentException("A valid unit visitor must be provided.");
		
		Integer key = nextUnitVisitorID();
		this.unitVisitors.put(key, unitVisitor);
		
		return key;
	}

	public Integer addSegmentVisitor(IXliffVisitor<Segment> segmentVisitor) {
		if (segmentVisitor == null)
			throw new IllegalArgumentException("A valid segment visitor must be provided.");
		
		Integer key = nextSegmentVisitorID();
		this.segmentVisitors.put(key, segmentVisitor);
		
		return key;
	}

	// Getters for visitors
	public List<IXliffVisitor<FileNode>> getFileNodeVisitors() {
		return new ArrayList<IXliffVisitor<FileNode>>(fileVisitors.values());
	}
	
	public List<IXliffVisitor<UnitNode>> getUnitNodeVisitors() {
		return new ArrayList<IXliffVisitor<UnitNode>>(unitVisitors.values());
	}
	
	public List<IXliffVisitor<Segment>> getSegmentVisitors() {
		return new ArrayList<IXliffVisitor<Segment>>(segmentVisitors.values());
	}
	
	
	// Removers for visitors
	public void removeFileVisitor(Integer ID) {
		if (ID == null || ID < 0)
			throw new IllegalArgumentException("A valid ID must be provided.");
		
		this.fileVisitors.remove(ID);
	}
	
	public void removeUnitVisitor(Integer ID) {
		if (ID == null || ID < 0)
			throw new IllegalArgumentException("A valid ID must be provided.");
		
		this.unitVisitors.remove(ID);
	}
	
	public void removeSegmentVisitor(Integer ID) {
		if (ID == null || ID < 0)
			throw new IllegalArgumentException("A valid ID must be provided.");
		
		this.segmentVisitors.remove(ID);
	}
	
	public void removeAllVisitors() {
		fileVisitors.clear();
		unitVisitors.clear();
		segmentVisitors.clear();
	}

	public XLIFFDocument getXlfDoc() {
		return this.xlf;
	}
	
	// Delegating methods to XLIFF doc
	public List<String> getFileNodeIds() {
		return this.xlf.getFileNodeIds();
	}

	public FileNode getFileNode(String fileNodeID) {
		if (fileNodeID == null || fileNodeID.isEmpty())
			throw new IllegalArgumentException("A valid file node ID must be provided.");
			
		return this.xlf.getFileNode(fileNodeID);
	}
	
	// Private helper methods
	private Integer nextFileVisitorID() {
		return ++fileVisitorID;
	}
	
	private Integer nextUnitVisitorID() {
		return ++unitVisitorID;
	}
	
	private Integer nextSegmentVisitorID() {
		return ++segmentVisitorID;
	}
}
