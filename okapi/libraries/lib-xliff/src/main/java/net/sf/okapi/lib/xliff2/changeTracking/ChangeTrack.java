/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.changeTracking;

import net.sf.okapi.lib.xliff2.Const;
import net.sf.okapi.lib.xliff2.core.BaseList;

/**
 * Represents the <code>&lt;changeTrack&gt;</code> element of the <a href=
 * 'http://docs.oasis-open.org/xliff/xliff-core/v2.0/xliff-core-v2.0.html#changeTracking_module'>Chan
 * g e Tracking module</a>.
 * 
 * @author Marta Borriello
 * 
 */
public class ChangeTrack extends BaseList<Revisions> {

	/** The tag element name constant. */
	public static final String TAG_NAME = "changeTrack";

	/**
	 * Creates a new {@link ChangeTrack} object.
	 */
	public ChangeTrack () {
		// Nothing to do
	}

	/**
	 * Copy constructor.
	 * @param original the original object to duplicate.
	 */
	public ChangeTrack (ChangeTrack original) {
		super(original);
	}

	/**
	 * Gets the complete opening <code>&lt;changeTrack&gt;</code> tag.
	 * 
	 * @param withNamespace
	 *            a boolean stating if the name space has to be included in the
	 *            tag.
	 * @return the complete <code>&lt;changeTrack&gt;</code> tag.
	 */
	public static String getCompleteOpeningTag(boolean withNamespace) {

		StringBuilder completeTag = new StringBuilder();
		completeTag.append("<");
		completeTag.append(Const.PREFIXCOL_TRACKINGSd);
		completeTag.append(TAG_NAME);
		if (withNamespace) {
			completeTag.append(" xmlns:");
			completeTag.append(Const.PREFIX_TRACKING);
			completeTag.append("=\"");
			completeTag.append(Const.NS_XLIFF_TRACKING20);
			completeTag.append("\"");
		}
		completeTag.append(">");
		return completeTag.toString();
	}

	/**
	 * Gets the <code>changeTrack</code> closing tag.
	 * 
	 * @return the <code>changeTrack</code> closing tag.
	 */
	public static String getClosingTag() {

		return "</" + Const.PREFIXCOL_TRACKINGSd + TAG_NAME + ">";
	}
}
