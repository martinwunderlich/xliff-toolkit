/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.metadata.Metadata;

/**
 * Provides the methods to add and retrieve metadata for an object. 
 */
public interface IWithMetadata {

	/**
	 * Indicates if the object has metadata.
	 */
	public boolean hasMetadata ();
	
	/**
	 * Gets the {@link Metadata} object for the parent, creates an empty of if there is none.
	 * @return the {@link Metadata} object for the parent (can be empty, but never null).
	 */
	public Metadata getMetadata ();

	/**
	 * sets the {@link Metadata} object for the parent.
	 * @param metadata the new {@link Metadata} object for the parent.
	 */
	public void setMetadata (Metadata metadata);

}
