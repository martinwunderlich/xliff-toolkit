/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.xliff2.core;

import net.sf.okapi.lib.xliff2.changeTracking.ChangeTrack;

/**
 * Represents an object that can contain a {@link ChangeTrack} object. Each
 * class representing a XLIFF element where the change tracking module is
 * allowed, should implement this interface.
 * 
 * @author Marta Borriello
 * 
 */
public interface IWithChangeTrack {

	/**
	 * Gets the {@link ChangeTrack} object for this element, creates an empty of if there is none.
	 * @return the {@link ChangeTrack} object for this element (can be empty, never null).
	 */
	public ChangeTrack getChangeTrack ();

	/**
	 * Sets the {@link ChangeTrack} object for this element.
	 * @param changeTrack the {@link ChangeTrack} object for this element.
	 */
	public void setChangeTrack (ChangeTrack changeTrack);

	/**
	 * Check if there is at least a revision for this element.
	 * @return <code>true</code> if there is at least a revision for this element; <code>false</code> otherwise.
	 */
	public boolean hasChangeTrack ();
}
