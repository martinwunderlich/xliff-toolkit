package net.sf.okapi.applications.lynx;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import net.sf.okapi.lib.xliff2.test.FileCompare;
import net.sf.okapi.lib.xliff2.test.StreamGobbler;
import net.sf.okapi.lib.xliff2.test.U;

import org.junit.Before;
import org.junit.Test;

public class LynxTest {
	
	private String[] javaLynx;
	private String root;
	private String appDir;
	private File rootAsFile;
	private FileCompare fc = new FileCompare();

	@Before
	public void setUp () throws URISyntaxException {
		File file = new File(getClass().getResource("/example.xlf").toURI());
		root = U.getDirectoryName(file.getAbsolutePath());
		rootAsFile = new File(root);

		// Set the path for the jar
		appDir = U.getDirectoryName(root); // Go up one dir
		appDir = U.getDirectoryName(appDir); // Go up one dir
		appDir = U.getDirectoryName(appDir); // Go up one dir
		appDir = U.getDirectoryName(appDir); // Go up one dir
		appDir += String.format("%sdeployment%smaven%sdist_xlifflib%s",
			File.separator, File.separator, File.separator, File.separator);
		javaLynx = new String[] { "java", "-jar", appDir + "lynx.jar" };
	}
	
    @Test
    public void testHelpCall () throws IOException, InterruptedException {
    	assertEquals(0, runLynx("-?"));
    }
    
    @Test
    public void testValidateExample () throws IOException, InterruptedException {
    	assertEquals(0, runLynx("-trace -v example.xlf"));
    }

    @Test
    public void testSamples () throws IOException, InterruptedException {
    	assertEquals(0, runLynx("-v "+appDir+"/samples/example.html.xlf"));
    	assertEquals(0, runLynx("-v "+appDir+"/samples/hello.html.xlf"));
    }

    @Test
    public void testRemoveAnnotations () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toRemoveAnnotations_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw -ra in-out/toRemoveAnnotations_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toRemoveAnnotations_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toRemoveAnnotations_in.out.xlf",
    		"in-out/toRemoveAnnotations_out.xlf", "UTF-8"));
    }

    @Test
    public void testRemoveExtensions () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toRemoveExtensions_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw -ra in-out/toRemoveExtensions_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toRemoveExtensions_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toRemoveExtensions_in.out.xlf",
    		"in-out/toRemoveExtensions_out.xlf", "UTF-8"));
    }

    @Test
    public void testSegment () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toSegment1_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw -seg in-out/toSegment1_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toSegment1_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toSegment1_in.out.xlf",
    		"in-out/toSegment1_out.xlf", "UTF-8"));
    }

    @Test
    public void testRewrite1 () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toRewrite1_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw in-out/toRewrite1_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toRewrite1_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toRewrite1_in.out.xlf",
    		"in-out/toRewrite1_out.xlf", "UTF-8"));
    }

    @Test
    public void testRewrite2 () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toRewrite2_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw in-out/toRewrite2_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toRewrite2_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toRewrite2_in.out.xlf",
    		"in-out/toRewrite2_out.xlf", "UTF-8"));
    }

    @Test
    public void testJoinSegments1 () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toJoin1_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw -join1 in-out/toJoin1_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toJoin1_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toJoin1_in.out.xlf",
    		"in-out/toJoin1_out.xlf", "UTF-8"));
    }

    @Test
    public void testJoinSegments2 () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toJoin2_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw -join1 in-out/toJoin2_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toJoin2_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toJoin2_in.out.xlf",
    		"in-out/toJoin2_out.xlf", "UTF-8"));
    }

    @Test
    public void testJoinSegments3 () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toJoin3_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw -join1 in-out/toJoin3_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toJoin3_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toJoin3_in.out.xlf",
    		"in-out/toJoin3_out.xlf", "UTF-8"));
    }

    @Test
    public void testJoinSegments4 () throws IOException, InterruptedException {
    	assertTrue(deleteOutputFile("in-out/toJoin4_in.out.xlf"));
    	assertEquals(0, runLynx("-trace -rw -join1 in-out/toJoin4_in.xlf"));
    	assertEquals(0, runLynx("-trace -v in-out/toJoin4_in.out.xlf"));
    	assertTrue(compareWithGoldFile("in-out/toJoin4_in.out.xlf",
    		"in-out/toJoin4_out.xlf", "UTF-8"));
    }

    @Test
    public void testSnippets () throws IOException, InterruptedException {
    	for ( int i=1; i<=14; i++ ) {
    		switch ( i ) {
    		case 14: // Expect an error
    			assertEquals(1, runLynx("-trace -x"+i));
    			break;
    		default: // Expect no error
        		assertEquals("Error on snippet #"+i, 0, runLynx("-trace -x"+i));
        		break;
    		}
    	}
    }

    private boolean compareWithGoldFile (String outputBase,
    	String goldBase,
    	String encoding)
    {
    	String outputPath = root + File.separator + outputBase;
    	String goldPath = root + File.separator + goldBase; 
    	return fc.compareFilesPerLines(outputPath, goldPath, encoding);
    }
    
    private boolean deleteOutputFile (String filename) {
    	File f = new File(root + File.separator + filename);
    	if ( f.exists() ) {
    		return f.delete();
    	}
    	else return true;
    }
    
//    private boolean deleteOutputDir (String dirname, boolean relative) {
//    	File d;
//    	if ( relative ) d = new File(root + File.separator + dirname);
//    	else d = new File(dirname);
//    	if ( d.isDirectory() ) {
//    		String[] children = d.list();
//    		for ( int i=0; i<children.length; i++ ) {
//    			boolean success = deleteOutputDir(d.getAbsolutePath() + File.separator + children[i], false);
//    			if ( !success ) {
//    				return false;
//    			}
//    		}
//    	}
//    	if ( d.exists() ) return d.delete();
//    	else return true;
//    }
    
    private int runLynx (String extraArgs) throws IOException, InterruptedException {
    	List<String> args = new ArrayList<String>();
    	for (String arg : javaLynx) {
    		args.add(arg);
    	}
    	for (String arg : extraArgs.split("\\s+")) {
    		args.add(arg);
    	}
    	System.out.println("");
    	System.out.println("cmd>===============================================================================");
    	System.out.println("cmd>"+((extraArgs==null) ? "" : " "+extraArgs));
    	Process p = Runtime.getRuntime().exec(args.toArray(new String[0]),
    		null, rootAsFile);
    	StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "err");            
    	StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "out");
    	errorGobbler.start();
    	outputGobbler.start();
    	return p.waitFor();
    }

}
